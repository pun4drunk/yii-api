<?php

/*
 * @category  Projects
 * @package   yii-api
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2015 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 *
 * @author vladislav
 */
namespace YiiApi\interfaces;

interface IApiRequest {
    public static function model($className=__CLASS__);
    public function countSpent($category, $from, $to = NULL);
    public function getLastTimestamp($attribute = 'created');
}
