<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of IApiUser
 *
 * @author vladislav
 */
namespace YiiApi\interfaces;

interface IApiUser {
    public function hasClientId($clientName = NULL);
    public function getToken($clientName);
    public function unsetToken($clientName);
}
