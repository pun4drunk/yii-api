<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiRatesBehavior
 *
 * @author vladislav
 */
namespace YiiApi\behaviors;
use YiiComponents\helpers\ArrayHelper;


class ApiRatesBehavior extends ApiEventsBehavior {
    

    public $category = "rates";
    
    public $floating = false;
    public $useDb = false;
    
    public $cacheKey = "rates";
    public $overloadCooldown = 900;
                
    
    public $rates = array();
    public $overloads = array();
    
    public $keyRemaining = 'remaining';
    public $keyLimit    = 'limit';
    public $keyReset    = 'reset';
    public $keyRate    = 'rate';
    public $interval = 3600;
    
    protected function dependencies() {
        return \CMap::mergeArray(parent::dependencies(), array(
            'apiRequest' => 'YiiApi\behaviors\ApiRequestBehavior',
        ));
    }
    
    public function attach($owner) {
        
        parent::attach($owner);
        
        if ($this->floating) {
            $this->useDb = true;
        }
        
        if (!$this->owner->requestClass) {
            
            if ($this->floating) {
                throw new \CException("Request class must be set to control floating rate limits!");
            }

            if ($this->useDb) {
                throw new \CException("Request class must be set to use db rate limits handling!");
            }    
        }
    
    }
    
    public function get($category = NULL, $forceUpdate = false) {
        
        if ($forceUpdate) {
            $this->updateRates();
        }
        
        $category = is_null($category) ? $this->apiRequest->get('category') : $category;
        $result = isset($this->rates[$category]) ? $this->rates[$category] : NULL;
        
        if (isset($this->overloads[$category])) {
            $timestamp = $this->overloads[$category];
            if (time() < $timestamp) {
                $result['remaining'] = 0;
            }
        }
        
        return $result;
    }
    
    public function setOverload($category, $timestamp) {
        $this->overloads[$category] = $timestamp + $this->overloadCooldown;
    }
    
    public function afterAssignTo() {
        $this->load();    
    }
    
    public function afterApiRequest() {
        
        $this->updateRates();
        
        if ($rate = $this->get()) {
            $this->logger->addInfo(
                    $this->getRateMessage($rate), 
                    $this->category.'.'.$this->apiRequest->get('category')
            );
        }
    }
    
    protected function getRateMessage($rate) {
        
        $message = "request limit info unknown";
        if (!is_null($rate)) {
            
            $remaining = (int)ArrayHelper::get($this->keyRemaining, $rate);
                    
            if ($remaining < 0) {
                throw new \CException("rate can not be less than zero: $remaining");
            }
            
            $reset = ArrayHelper::get($this->keyReset, $rate);
            $limit = ArrayHelper::get($this->keyLimit, $rate);
            
            if (strlen($reset)) {
                $message = vsprintf("%d of %d requests left, reset on %s", array(
                    $remaining, $limit, date($this->dateFormat, $reset),
                ));    
            } else {
                $message = vsprintf("%d of %d requests left", array(
                    $remaining, $limit,
                ));
            }
                
        }
        
        return $message;
        
    }
    
    protected function updateRates() {
        
        if (!$this->useDb) {
            $this->saveWithCache();
        } else {
            $this->loadWithDb($this->apiRequest->get('category'));
        }
        
        $result = (array) $this->apiRequest->get('result');
        if ($rate = ArrayHelper::get($this->keyRate, $result, false)) {
            $this->rates[$this->apiRequest->get('category')] = $rate;
        }
    }
    
    protected function load() {
    
        if ($this->useDb) {
            $this->loadWithDb();
        } else {
            $this->loadWithCache();
        }
    }
    
    protected function saveWithCache() {
        \Yii::app()->cache->set($this->getCacheKey(), $this->rates);
    }
    
    protected function loadWithCache() {
        $this->rates = \Yii::app()->cache->get($this->getCacheKey());
    }
    
    protected function loadWithDb($category = NULL) {
        
        if (is_null($category)) 
        {
            foreach ($this->rates as $category => $rate) 
            {
                $this->loadWithDb($category);
            }
        } 
        else 
        {
            if (isset($this->rates[$category])) 
            {
                $requestClass = $this->owner->requestClass;
                
                $limit = $this->rates[$category][$this->keyLimit];
                $spent = $requestClass::model()->countSpent($category, $this->interval);
                
                $this->rates[$category][$this->keyRemaining] = $limit - $spent;
            }
            
        }
    }
    
}
