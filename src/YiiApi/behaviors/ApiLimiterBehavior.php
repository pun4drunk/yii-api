<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of ApiLimiterBehavior
 *
 * @author vladislav
 */
namespace YiiApi\behaviors;
use YiiComponents\helpers\ArrayHelper;

class ApiLimiterBehavior extends ApiEventsBehavior {

    public $category = 'limiter';
    public $sleep = 10;
    
    public $safeDelta = 5;
    
    protected function dependencies() {
        return \CMap::mergeArray(parent::dependencies(), array(
            'apiRequest' => 'YiiApi\behaviors\ApiRequestBehavior',
            'apiRates'   => 'YiiApi\behaviors\ApiRatesBehavior',
        ));
    }
    
    public function beforeApiRequest() {
        
        $rate = (array)$this->apiRates->get();
        
        if (!$rate) {
            $this->logger->addWarning("failed to get request rate for ".$this->apiRequest->get('category'), $this->category);
            return true;
        }
        
        if ((int)ArrayHelper::get($this->apiRates->keyRemaining, $rate) < 1) {
            
            $reset = ArrayHelper::get($this->apiRates->keyReset, $rate);
                    
            if (strlen($reset)) {
                
                $reset += $this->safeDelta;
                $message = sprintf("request limit exceeded, need to sleep until %s", date($this->dateFormat, $reset));
                $seconds = $reset - time();
                
            } else {
                
                $message = sprintf("request limit exceeded, need to sleep for %s", $this->sleep);
                $seconds = $this->sleep;
                
            }

            $this->logger->addWarning($message, $this->category);
            sleep($seconds);
            
            return $this->beforeApiRequest();
            
        }
        
        return true;
    }
    
    
    
    
}
