<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiEventsBehavior
 *
 * @author vladislav
 */
namespace YiiApi\behaviors;

class ApiEventsBehavior extends ApiBehavior {
    
    public function events(){
        return array(
            'onAfterAssignTo'   => 'afterAssignTo',
            'onBeforeApiRequest'=>'beforeApiRequest',
            'onAfterApiRequest' =>'afterApiRequest',
        );
    }
    
    public function afterAssignTo() {
        return;
    }
    
    public function beforeApiRequest() {
        return true;
    }
    
    public function afterApiRequest() {
        return;
    }

}
