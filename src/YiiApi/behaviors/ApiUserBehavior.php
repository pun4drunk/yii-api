<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of ApiUserBehavior
 *
 * @author vladislav
 */
namespace YiiApi\behaviors;
use YiiComponents\behaviors\DependentBehavior;

abstract class ApiUserBehavior extends DependentBehavior {
    
    const ENTITY_CLIENTID = 'client_id';
    const ENTITY_TOKEN = 'token';
    
    public $tableName = 'table_eav';
    public $clientIdFormat = '%s.client_id';
    public $tokenFormat = '%s.token';
    public $uniqueClientId = true;
    
    protected function dependencies() {
        return array(
            'eav' => "YiiComponents\behaviors\EavExtendedBehavior",
        );
    }
    
    protected function getCustomMethodName($action, $name, $entity) {
        return strtolower($action).ucfirst($name).ucfirst($entity);
    }
    
    protected function getAttributeName($name, $entity) {
        
        switch ($entity) {
            case self::ENTITY_CLIENTID:
                $attribute = vsprintf($this->clientIdFormat, array($name));
                break;
            
            case self::ENTITY_TOKEN:
                $attribute = vsprintf($this->tokenFormat, array($name));
                break;
            
            default:
                throw new CException("Unknown entity: $entity");
        }
        
        return $attribute;
    }
    
    public function setToken($name, $args) {
        $methodName = $this->getCustomMethodName("set", $name, self::ENTITY_TOKEN);
        return method_exists($this, $methodName) ? call_user_func_array(array($this, $methodName), $args) : $this->_setToken($name, $args);
    }
    
    public function unsetToken($name) {
        $methodName = $this->getCustomMethodName("unset", $name, self::ENTITY_TOKEN);
        return method_exists($this, $methodName) ? $this->$methodName() : $this->_unsetToken($name);
    }
    
    public function getToken($name) {
        $methodName = $this->getCustomMethodName("get", $name, self::ENTITY_TOKEN);
        return method_exists($this, $methodName) ? $this->$methodName() : $this->_getToken($name);
    }
    
    
    public function setClientId($name, $client_id) {
        
        //if client id must be unique
        if ($this->uniqueClientId 
                && !$this->isUniqueClientId($name, $client_id)) {
            throw new \CException("This client is already registered with another account. If you believe it must be a mistake, please contact site administrator.", 400);
        }
        $attribute = $this->getAttributeName($name, self::ENTITY_CLIENTID);
        return $this->eav->setEavAttribute($attribute, $client_id, true);
    }
    
    protected function _getToken($name) {
        $attribute = $this->getAttributeName($name, self::ENTITY_TOKEN);
        return $this->eav->getEavAttribute($attribute);
    }
    
    protected function _setToken($name, $value) {
        $attribute = $this->getAttributeName($name, self::ENTITY_TOKEN);
        return $this->eav->setEavAttribute($attribute, $value, true);
    }

    protected function _unsetToken($name) {
        $attribute = $this->getAttributeName($name, self::ENTITY_TOKEN);
        return $this->eav->setEavAttribute($attribute, NULL, true);
    }
    
    public function isUniqueClientId($name, $client_id) {
        $attribute = $this->getAttributeName($name, self::ENTITY_CLIENTID);
        return $this->eav->isUniqueValue($attribute, $client_id);
    }
    
    public function getClientId($name = NULL) {
        
        $result = NULL;
        if ($name) {
            $attribute = $this->getAttributeName($name, self::ENTITY_CLIENTID);
            $result = $this->eav->getEavAttribute($attribute);
        }
        return $result;
    }
    
    public function unsetClientId($name = NULL) {
        $result = NULL;
        if ($name) {
            $attribute = $this->getAttributeName($name, self::ENTITY_CLIENTID);
            $result = $this->eav->setEavAttribute($attribute, NULL, true);
        }
        return $result;
    }
    
    public function hasClientId($name = NULL) {
        return (boolean) ($this->getClientId($name));
    }
    
}
