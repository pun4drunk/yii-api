<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiRequestModelBehavior
 *
 * @author vladislav
 */
namespace YiiApi\behaviors;
use YiiComponents\helpers\AppHelper;

class ApiRequestModelBehavior extends ApiEventsBehavior {
    
    protected $model;
    
    public function getModel() {
        return $this->model;
    }
    
    protected function dependencies() {
        return \CMap::mergeArray(parent::dependencies(), array(
            'apiRequest' => 'YiiApi\behaviors\ApiRequestBehavior',
        ));
    }
    
    public function init() {
        $this->model = NULL;
    }
    
    public function beforeApiRequest() {
        
        //init
        $this->init();
        
        $this->model = new $this->owner->requestClass;
        foreach (array('url', 'name', 'method', 'category') as $name) {
            $this->model->$name = $this->apiRequest->get($name);
        }

        $this->model->params = json_encode($this->apiRequest->get('params'));         
        $this->model->created = AppHelper::getDbTimestamp(time(), $this->model->getDbConnection(), true);  


        if (!$this->model->save()) {
            throw new \CException("Api Request save error: ".json_encode($this->model->errors), 500);
        }    
               
        return;
    }
    

    public function afterApiRequest() {

        $update = array('status', 'duration');
            
        foreach ($update as $name) {
            $this->model->$name = $this->apiRequest->get($name);
        }
            
        if (!$this->model->update($update)) {
            throw new \CException("Api Request update error: ".json_encode($this->model->errors), 500);
        }
        
    }
    
}
