<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of ApiLoggerBehavior
 *
 * @author vladislav
 */
namespace YiiApi\behaviors;

class ApiLoggerBehavior extends ApiEventsBehavior {
    
    protected $attributes = array();
    public $category = 'request.log';
    
    protected function dependencies() {
        return \CMap::mergeArray(parent::dependencies(), array(
            'apiRequest' => 'YiiApi\behaviors\ApiRequestBehavior',
        ));
    }

    
    public function afterApiRequest() {

        
        $attributes = $this->apiRequest->getAttributes();
        
        if ($params = $attributes['params']) {
            $params = serialize($params);
        }
        
        $message = "{$attributes['method']} {$attributes['url']}{$params}:[{$attributes['status']}]";
        $this->logger->addInfo($message, "$this->category.{$attributes['category']}");
    }
    
    
}
