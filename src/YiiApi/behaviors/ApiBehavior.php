<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of ApiBehavior
 *
 * @author vladislav
 */
namespace YiiApi\behaviors;

use YiiApi\components\ApiBase;
use YiiComponents\behaviors\DependentBehavior;

abstract class ApiBehavior extends DependentBehavior {
    
    public $category;
    public $dateFormat = "H:i:s";
    public $cacheKey;

    protected function dependencies() {
        return array(
            'logger' => 'YiiComponents\behaviors\LoggerBehavior',
        );
    }

    public function attach($owner) {
        
        if (!$owner instanceof ApiBase) {
            throw new \CException(get_class($owner)." is not an instance of ApiBase");
        }
        
        parent::attach($owner);
    }
    
    protected function getCacheKey() {
        $this->cacheKey = is_null($this->cacheKey) ? __CLASS__ : $this->cacheKey;
        return "api.{$this->owner->clientName}.{$this->cacheKey}";
    }
        
}
