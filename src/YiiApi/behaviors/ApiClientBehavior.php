<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiClientBehavior
 *
 * @author Vladislav
 */
namespace YiiApi\behaviors;
use CBehavior;
use CException;

class ApiClientBehavior extends CBehavior {
    
    public $client_id;
    
    public $parent;
    
    public $name = 'client';
    public $user_id;
    
    protected $client;
    public $requireAuth = true;
    public $noAuthMethod;
    public $noAuthArgs = array();
    public $clientClass = 'YiiApi\components\Api';
    public $parentClass = 'CModule';
        
    public function getClient($force = false) {
        
        if (is_null($this->client) || $force) {
         
            if (!$this->parent instanceof $this->parentClass) {
                throw new CException("'parent' property ".  get_class($this)." must be an instance of $this->parentClass");
            }
        
            if (!$this->name) {
                throw new CException("name must be defined to use ".  get_class($this), 500);
            }
            
            $this->client = $this->parent->getComponent($this->name);
            
            if (!$this->client instanceof $this->clientClass) {
                throw new CException("Application/module component '$this->name' must be an instance of $this->clientClass to be configured by ".  get_class($this));
            }
            
            if (!$this->client_id) {
                return $this->requireAuth ? $this->clientIsNotAuthorized() : $this->client;            
            }
            
            $this->client->assignTo($this->user_id);
    
            if (!is_object($this->client)) {
                $this->client = false;
            }
            
            if (!$this->client) {
                throw new CException("Api client was not initialized. Please contact system administrator", 500);
            }
    
            if (!$this->client->isAuthorized) {
                return $this->requireAuth ? $this->clientIsNotAuthorized() : $this->client;
            }
            
        }
        
        return $this->client;
    }
    
    public function getClientId() {
        return $this->client_id;
    }
    
    protected function clientIsNotAuthorized() {
        
        $result = false;
        if ($this->noAuthMethod) {
            $result = call_user_func_array($this->noAuthMethod, $this->noAuthArgs);
        }
        return $result;
    }
    
}
    