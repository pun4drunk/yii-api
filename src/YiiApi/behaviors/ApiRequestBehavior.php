<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of ApiLoggerBehavior
 *
 * @author vladislav
 */
namespace YiiApi\behaviors;

use YiiComponents\helpers\ArrayHelper;

class ApiRequestBehavior extends ApiBehavior {
    
    protected $attributes = array();
    
    public function getAttributes() {
        return $this->attributes;
    }
    
    public function get($name, $default = NULL) {
        return ArrayHelper::get($name, $this->attributes, $default);
    }
    
    public function unsetAttributes() {
        $this->attributes = array();
        return $this;
    }
    
    public function setAttributes(array $attributes) {
        $this->attributes = array_merge($this->attributes, $attributes);
        return $this;
    }
    
    public function set($name, $value = NULL) {
        $this->attributes[$name] = $value;
        return $this;
    }
    
    
    
}
