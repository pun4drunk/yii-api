<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of AutoDelayBehavior
 *
 * @author vladislav
 */
namespace YiiApi\behaviors;

use YiiComponents\helpers\AppHelper;
use YiiComponents\helpers\ActiveRecordHelper;

class ApiDelayBehavior extends ApiEventsBehavior {
    
    public $category = 'delay';
    
    public $autoDelayMin = 1;
    public $autoDelayMax = 3;
    
    public $timestampField = 'created';
    
    protected $lastTimestamp;
    public $cacheKey = "lastTimestamp";

    public function getLastTimestamp() {
        
        $timestamp = false;
        if ($this->owner->requestClass) {
            $timestamp = $this->getLastDbTimestamp();
        } else {
            if (is_null($this->lastTimestamp)) {
                
                if ($this->lastTimestamp = \Yii::app()->cache->get($this->getCacheKey())) {
                    $this->logger->addTrace("timestamp $this->lastTimestamp retrieved from cache", $this->category);
                }
                
            }
            
            $timestamp = $this->lastTimestamp;
        }
        
        return $timestamp;
    }
    
    protected function getLastDbTimestamp() {
        $requestClass = $this->owner->requestClass;
        $timestamp = $requestClass::model()->getLastTimestamp($this->timestampField);
        return $timestamp;
    }
    
    protected function setLastTimestamp($timestamp) {
        if (!$this->owner->requestClass) {
            $this->lastTimestamp = $timestamp;
            \Yii::app()->cache->set($this->getCacheKey(), $timestamp);
        }
    }
    
    public function beforeApiRequest() {
        
        $timestamp = time();
        
        if ($lastTimestamp = $this->getLastTimestamp()) {
            
            $delay = $timestamp - $lastTimestamp;
            
            if ($delay < 0) {
                throw new \CException("Delay can not be less than zero! Something went wrong");
            }
            
            if ($delay < $this->autoDelayMin) {
                $delay = rand($this->autoDelayMin, $this->autoDelayMax - $delay);
                
                $this->logger->addInfo("sleeping for $delay second(s)... ", $this->category);
                sleep($delay);
                
            } else {
                $this->logger->addTrace("current delay is $delay, no need to sleep this time", $this->category);
            }
        } else {
            
            $this->logger->addTrace("first api request, no need to sleep this time", $this->category);
            
        }
        
        $this->setLastTimestamp($timestamp);
        
        return true;
    }
}
