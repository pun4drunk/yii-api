<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of EApi
 *
 * @author vladislav
 */

namespace YiiApi\components;
use YiiApi\exceptions\ApiFatalException;

abstract class Api extends ApiBase {
    
    const HTTP_METHOD_GET = 'GET';
    const HTTP_METHOD_POST = 'POST';
    
    const HTTP_STATUS_ZERO = 0;
    const HTTP_STATUS_OK = 200;
    const HTTP_STATUS_TOOMANYREQUESTS = 429;
    const HTTP_STATUS_DENIED = 401;
    const HTTP_STATUS_BADREQUEST = 400;
    
    public $clientConfig;
    
    //current user client id
    public $clientId;
    
    public $userClass = 'User';
    public $httpOptions = array();
    public $isAuthorized = false;
    
    public $logPrefix = 'username';
    
    public $requestClass;
    public $requestClassImplements = array('YiiApi\interfaces\IApiRequest');
    
    protected $requestDurationPrecision = 4;

    public function init() {
        
        $this->_initialized = false;
        
        if ($this->requestClass) {
            
            if ($diff = array_diff(
                    $this->requestClassImplements, 
                    array_merge(
                        class_parents($this->requestClass), 
                        class_implements($this->requestClass)
                    ))) 
            {
                throw new \CException("$this->requestClass must implement ".implode(", ", $diff));
            }
        }
        
        return parent::init();
    }
    
    protected function internalBehaviors() {
        $behaviors = parent::internalBehaviors();
        if ($this->requestClass) {
            $behaviors['apiRequestModel'] = array(
                'class' => 'YiiApi\behaviors\ApiRequestModelBehavior',
            );
        }
        return $behaviors;
    }
    
    public function getRequestModel() {
        return $this->requestClass ? $this->asa('apiRequestModel')->getModel() : NULL;
    }
    
    public function assignTo($user_id) {
        
        $this->attachBehavior('userBehavior', array(
            'class' => 'YiiComponents\behaviors\UserBehavior',
            'userClass' => $this->userClass,
            'user_id' => $user_id,
        ));
        
        $user = $this->asa('userBehavior')->getUser();
        
        $logPrefix = $user->{$this->logPrefix};
        $this->logger->category = "$this->logCategory.$logPrefix";
        
        $this->authorizeUser();
        return parent::assignTo($user_id);
    }
    
    protected function authorizeUser() {
        $this->isAuthorized = true;
        //put some code in child classes
        return $this->isAuthorized;
    }

    protected function beforeApiRequest() {
        
        $this->apiRequest->set('start', microtime(true));
        return parent::beforeApiRequest();
    }
    
    protected function afterApiRequest() {
        
        $end = microtime(true);
        $this->apiRequest->set('duration', 
                round($end - $this->apiRequest->get('start'), $this->requestDurationPrecision));
        
        $this->processResult();
        $this->checkRequestHttpStatus();
        
        parent::afterApiRequest();
    }
    
    protected function processResult() {
        $result = (array) $this->apiRequest->get('result');
        
        $status = isset($result['httpstatus']) ?  (int)$result['httpstatus'] : NULL;
        $this->apiRequest->set('status', $status);
        
    }
    
    protected function checkRequestHttpStatus() {
    
        $status = $this->apiRequest->get('status');
        
        switch ($status) {
            case self::HTTP_STATUS_OK:
                break;
            
            case self::HTTP_STATUS_ZERO:
                throw new ApiFatalException("Connection error", $status);
                break;
            
            case self::HTTP_STATUS_BADREQUEST:
                $result = $this->apiRequest->get('result');
                throw new ApiFatalException(json_encode($result), $status);
                break;
            
            default:
                $this->handleRequestResultException();
                $this->apiRequest->set('result', NULL);
                break;
        }
    }
    
    protected function handleRequestResultException() {
        $message = json_encode($this->apiRequest->get('result'));
        throw new \CException("api request failed: $message");
    }
}
