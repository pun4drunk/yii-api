<?php

/*
 * @category  Projects
 * @package   yii-api
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2015 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of ApiBase
 *
 * @author vladislav
 */

namespace YiiApi\components;
use \CApplicationComponent;
use \CEvent;

abstract class ApiBase extends CApplicationComponent {
    
    protected $_initialized = false;
    protected $_api;

    protected $requestMethod;
    
    public $logCategory = 'api';

    protected function internalBehaviors() {
        return array(
            'logger' => array(
                'class' => 'YiiComponents\behaviors\LoggerBehavior',
                'category' => $this->logCategory,
            ),
            'apiRequest' => array(
                'class' => 'YiiApi\behaviors\ApiRequestBehavior',
            ),
        );
    }
    
    abstract protected function getApi();

    public function init() {
        $this->_initialized = false;
        $this->attachBehaviors(\CMap::mergeArray($this->internalBehaviors(), $this->behaviors));
        
        if ($this->getApi()) {
            $this->_initialized = true;
        }
        
        return $this->_initialized;
    }
    
    public function isRawAction($name) {
        return false;
    }
    
    public function __call($name, $params) {
        
        //avoid processing native api methods that dont include actual requests
        if ($this->isRawAction($name)) {
            return call_user_func_array(array($this->_api, $name), $params);
        }
        
        //proxy all requests to actual api
        $this->apiRequest->unsetAttributes()->setAttributes(array(
            'name'   => $name,
            'params' => $params,
            'result' => NULL,
        ));
        
        $result = false;
        
        if (!$this->beforeApiRequest()) {
            return false;
        }

        $this->apiRequest->set('result', $this->doApiRequest());
        $this->afterApiRequest();

        $result = $this->apiRequest->get('result');
        $this->apiRequest->set('result', NULL);
        
        return $result;
    }

    protected function doApiRequest() {
        return call_user_func_array(array($this->_api, $this->apiRequest->get('name')), $this->apiRequest->get('params'));
    }

    public function assignTo($user_id) {
        $this->afterAssignTo();
        return true;
    }
    
    public function onAfterAssignTo($event) {
        $this->raiseEvent('onAfterAssignTo', $event);
    }
    
    public function onBeforeApiRequest($event) {
        $this->raiseEvent('onBeforeApiRequest', $event);
    }
    
    public function onAfterApiRequest($event) {
        $this->raiseEvent('onAfterApiRequest', $event);
    }
    
    protected function afterAssignTo() {
        $event = new CEvent($this);
        $this->onAfterAssignTo($event);
    }
    
    protected function beforeApiRequest() {
        
        $event = new CEvent($this);
        $this->onBeforeApiRequest($event);
        return true;
    }
    
    protected function afterApiRequest() {
        $event = new CEvent($this);
        $this->onAfterApiRequest($event);
    }
    
    
}
